# Generated by Django 4.1.2 on 2022-12-14 04:43

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Appointment', '0007_slots_dates'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='slots',
            name='dates',
        ),
    ]
