# Generated by Django 4.1.2 on 2022-12-14 04:44

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('Appointment', '0008_remove_slots_dates'),
    ]

    operations = [
        migrations.AddField(
            model_name='slots',
            name='dates',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='Appointment.available_dates'),
        ),
    ]
