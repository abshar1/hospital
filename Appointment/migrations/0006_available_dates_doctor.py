# Generated by Django 4.1.2 on 2022-12-14 04:11

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('Appointment', '0005_available_dates'),
    ]

    operations = [
        migrations.AddField(
            model_name='available_dates',
            name='doctor',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='Appointment.doctor'),
        ),
    ]
